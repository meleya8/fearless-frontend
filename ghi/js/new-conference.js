window.addEventListener('DOMContentLoaded', async () => {
    const url =  'http://localhost:8000/api/locations/'

    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        console.log(data)
        
        const selectTag = document.getElementById('state')
        
        for (let location of data.locations) {
            const option = document.createElement('option')
            option.value = location.pk
            option.innerHTML = location.href
            selectTag.appendChild(option)
        }
    }
})