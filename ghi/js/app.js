function createCard(name, description, pictureUrl) {
    
    return `
        <div class="card">
            <img src= "${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
                <p class="card-text">${description}</p>
            </div>
        </div>
    `;
  }
window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            console.log("My Error")
        } else {
            const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                  const details = await detailResponse.json();
                //   console.log("My details", details)
                  const name = details.conference.name;
                  const description = details.conference.description;
                  const pictureUrl = details.conference.location.picture_url;
                  const html = createCard(name, description, pictureUrl);
                
                  
                  const column = document.querySelector('.col');
                  column.innerHTML += html;
                }
            }
        }

    } catch (e) {
        //raised an error
    }

});

            {/* // const conference = data.conferences[1];
            // const nameTag = document.querySelector('.card-title');
            // nameTag.innerHTML = conference.name;

            // const detailUrl = `http://localhost:8000${conference.href}`;
            // const detailResponse = await fetch(detailUrl);
            // if (detailResponse.ok) { */}
                {/* //   const details = await detailResponse.json();
                //   console.log("My details", details);
                
                //   const description = details.conference.description
                //   const nameTag = document.querySelector('.card-text')
                //   nameTag.innerHTML = description
                // //   console.log("My description", description)
                
                //   const imgTag = document.querySelector('.card-img-top')
                //   imgTag.src = details.conference.location.picture_url
                //   imgTag.innerHTML = imgTag.scr
                //   console.log("My img", imgTag.src)
                
                //   const exa = createCard(conference.name, description, imgTag.src)
                //   console.log(exa)
                // } */}
                
                