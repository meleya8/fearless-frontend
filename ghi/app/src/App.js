import LocationForm from './LocationForm';
import PresentationForm from './PresentationForm';
import Nav from './Nav';
// import AttendeesList from './AttendeesList';
import { BrowserRouter } from "react-router-dom";
import { Routes, Route } from "react-router-dom";


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
          <Routes>
            <Route index element={<MainPage />} />
            {/* <Route path="conferences/new" element={<ConferenceForm />} /> */}
            {/* <Route path="attendees/new" element={<AttendConferenceForm />} /> */}
            <Route path="locations/new" element={<LocationForm />} />
            <Route path="presentation/new" element={<PresentationForm />} />
            {/* <Route path="attendees" element={<AttendeesList attendees={props.attendees} />} /> */}
          </Routes>
      </div>
    </BrowserRouter>
  );
}
  // return (
//     <>
//       <Nav />
//       <div className="container">
//         <LocationForm />
//         {/* <AttendeesList attendees={props.attendees} /> */}
//       </div>
//     </>
//   );
// }

export default App;
// function App(props) {
//   if (props.attendees === undefined) {
//     return null;
//   }
//   return (
//     <div className="container">
//       <table>
//         <thead>
//           <tr>
//             <th>Name</th>
//             <th>Conference</th>
//           </tr>
//         </thead>
//         <tbody>
//           {props.attendees.map(attendee => {
//             return ( 
//               <tr key={attendee.href}>
//                 <td>{ attendee.name }</td>
//                 <td>{ attendee.conference }</td>
//               </tr>
//               );
//           })};
//         </tbody>
//       </table>
//     </div>
//   );
// }

// export default App;